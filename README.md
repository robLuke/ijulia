# Neuroscience Jupyter

Jupyter setup configured to run:

* python2
* python3
* R
* julia

With useful libraries installed including MNE (python), EEG.jl (julia), ggplot (R) etc

## Instructions

### Post Install Requirements

Currently there are a few bugs in the install so you must run the following commands post install.
Open the jupyter window and start a new terminal and enter:

* `pip2 install -e git+https://github.com/mne-tools/mne-python#egg=mne-dev`
* `julia -e 'Pkg.build("IJulia")'`

Refresh the window and you should be good to go.

### Server

This is tested on Amazon AWS EC2.
Pair with S3 for fast data analysis.

* docker run -p 8888:8888 -v /home/core:/workspace codles/jupyter-with-julia-r

### Desktop

Search for 'codles' in [Kitematic](https://kitematic.com/)